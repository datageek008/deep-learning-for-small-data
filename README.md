# Image Classification

# Image Preprocessing
1. Training and test dataset images was trimmed by removing the upper text by 250:628 from height
2. Neural networks need standard sized images, so we resize the images to 224*224  
3. Mean Normalisation to centre the images
4. One hot encoding of the training and test labels is done


# Neural Network Architecture
##### 1.Convolutional Layer  
  We add 64 2 Dimensional convolutional layer to understand visual representations with kernel size of (3,3) 
  which scans the inputs with padding of 0 and stride of 1.
We also apply a non linearity using relu activations
##### 2 .MaxPool Layer  
   A 2D max-pool layer of size (2,2) which downsamples the input size    
##### 3. Flatten layer 
   To flatten the data to be used for dense layer
##### 4. Dense layer
   A fully connected layer, where each neuron will be fully connected to the next layer Usually in the dimension of power of 2, We use 32 in our network
##### 5. Batch Normalization Layer  
   Added between linear and non linear layer and normalizes the input to our activation function
##### 6. DropOut
   Dropout acts a a regularization, because by randomly disabling a fraction of neurons in the layer (set to 50% here) it ensure that that model does not overfit. This prevents neurons from adapting together and forces them to learn individually useful features.
##### 7. Dense layer with SoftMax Activation Layer  
   To normalize the results into probability scores, and all values will add up to 1 
   

##### Loss Function
Cross Entropy loss is used as a loss function because each text will output to one value 
Also,we use Adam as a regularize because of its computational efficiency and ease of convergence

##### Unbalanced classes 
  Because the input data is imbalanced and the ratio of A:B:C classes is 7:5:11,                                             
  we provide class weights
  for the model to train on {0: 1.0952380952380953, 1: 1.5333333333333334, 2: 0.696969696969697} 
  We test our data on 5 images,(2 of C,2 of A,1 of B)

##### Data Augmentation - Because the number of images trained is very small, and we need training data for neural network,
We can generate new training data by augmenting the existing images by performing
horizontal flip,shearing,featurewise centre


# Results & Improvements
For a Sample Image the model correctly predicts the output in output.txt file (0 -Class A,1-Class B,2-Class C)

Ex Output.txt -> predicts 2 when the input sample is from class C

##### Attached files - 
accuracy.png - Accuracy on test set vs Epoch
loss.png     - Loss on test set vs Epoch

